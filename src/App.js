import React,{Component} from 'react';
import {BrowserRouter,Route} from 'react-router-dom';


import Table from './components/Table';
import Editar from './components/Editar';

console.clear();
export default class App extends Component{


  render(){
    return(
      <div className="container" id="principal">
        <br/>
        <div className="row">
        <div class="col s12 m12 l12">
            {/* this.state.inp.tableShow  
              ? <Table inps={this.getShowTable} />
            : <div></div> */}
            <BrowserRouter>
            <React.Fragment>
                <Route exact path="/" component={Table} />
                <Route exact path="/editar/:id" component={Editar} />
                {/*<Route exact path="/nosotros" component={Nosotros} />
                <Route exact path="/galeria" component={Galeria} /> */}
              </React.Fragment>
            </BrowserRouter>
          </div>
        </div>
      </div>
    );
  }
}




