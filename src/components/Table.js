import React,{Component} from 'react';
import data from '../conf/data';

import axios from 'axios';
import { Link } from "react-router-dom";


export default class Table extends Component{

    constructor(props) {
        super(props);
        
        this.state = {
          propiedades:[]
        };
    }
    
      
    componentDidMount() {
        axios.get(`${data.url}propiedades`,{})
        .then(res => {
          const propiedades = res.data;
          this.setState({ propiedades });
        });
    }


    EditPrecio(id){
        console.log(id);
    }


    render(){
        console.log(this.state.propiedades);
        return(
            <div>
                <h3 className="header">Lista de propiedades</h3>
                <table className="highlight centered responsive-table">
                    <thead>
                        <tr>
                            <th>Direcion</th>
                            <th>Num Ext</th>
                            <th>Num Int</th>
                            <th>Delegacion</th>
                            <th>Ciudad</th>
                            <th>Precio</th>
                            <th>Nombre Propietario</th>
                            <th>Telefono</th>
                            <th>Correo</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.propiedades.map((element,index) => (<tr key={index}> 
                            <td>{element.direccion}</td>
                            <td>{element.numero_ext}</td>
                            <td>{element.numero_int}</td>
                            <td>{element.delegacion}</td>
                            <td>{element.ciudad}</td>
                            <td>{element.precio}</td>
                            <td>{element.nombre} {element.apPaterno}</td>
                            <td>{element.telefono}</td>
                            <td>{element.email}</td>
                            {/* <td><button onClick={ ()=> this.EditPrecio(element.id)}>Editar Precio</button></td> */}
                            <td><Link to={`editar/${element.id}`} className="waves-effect waves-light btn">Editar</Link></td>
                        </tr>) )}
                    </tbody>
                </table>
            </div>
        );
    }
    

}