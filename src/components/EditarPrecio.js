import React,{Component} from 'react';
//import {Router,Route,browserHistory} from 'react-router';

import data from '../conf/data';

import axios from 'axios';

/* console.clear(); */
export default class EditarPrecio extends Component{

    constructor(props){
        super(props);
        const idProp = this.props.idPropietario;
        this.state={
            idPropietario:idProp,
            isUpdate:0,
            precio:''           
        };
    }


    componentDidMount() {
        axios.get(`${data.url}propiedades/${this.props.idPropietario}`,{})
        .then(res => {
          const propiedad = res.data;
          this.setState({ precio:propiedad.precio });
        });
    
    }


    EditPropietario(id){
        this.props.precioParentUpdate(false);
    }
    
    handleSubmit = (e) => {
        /* const options = {
            headers: { 'Content-Type': 'application/json' },
          };
        axios(options);
         */
        const obj = {id:this.state.idPropietario,
                    precio:this.state.precio
                };
        
        e.preventDefault();
        var self=this;
        axios.post(`${data.url}propiedades/precio/editar`,obj)
        .then(res => {
            const respuesta = res.data;
            console.log('respuesta update',respuesta);
            //self.setState({isUpdate:respuesta.isUpdate});
            if(respuesta.isUpdate==true){
                self.props.precioParentUpdate(false); /** cambia el estado en el padre */  
            }
        });
        
        //
        
    }


    render(){
        return(
            <div className="container">
                <div className="col s12 m12">
                <h2 className="header">Precio</h2>
                    <div className="card horizontal">
                        <div className="card-stacked">
                        <form onSubmit={this.handleSubmit}>
                            <div className="card-content">
                            <p>
                                <label htmlFor='precio'>Precio : </label>
                                <input 
                                    id='precio'
                                    name='precio'
                                    placeholder='Precio'
                                    onChange={e=>this.setState({precio:e.target.value})}
                                    ref={inputElement=>this.inputName=inputElement}
                                    value={this.state.precio}
                                />
                            </p>
                            </div>
                            <div className="card-action"><button className="waves-effect waves-light btn">Finalizar</button></div>
                        </form>
                    </div></div>
                {/*<button onClick={ ()=> this.EditPropietario(this.props.idPropietario)}>Editar Propietario</button>*/}
            </div></div>
        );
    }
    

}