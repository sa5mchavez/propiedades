import React,{Component} from 'react';

import data from '../conf/data';

import axios from 'axios';

/* console.clear(); */
export default class EditarPropietario extends Component{

    constructor(props){
        super(props);
        const idProp = this.props.idPropietario;
        this.state={
            idPropietario:idProp,
            isUpdate:0,
            nombre:'',
            apPaterno:'',
            telefono:'',
            email:''           
        };
    }


    componentDidMount() {
        axios.get(`${data.url}propiedades/${this.props.idPropietario}`,{})
        .then(res => {
          const propiedad = res.data;
          this.setState({ nombre:propiedad.nombre, apPaterno:propiedad.apPaterno,
                        telefono:propiedad.telefono,email:propiedad.email });
        });
    
    }


    EditPropietario(id){
        this.props.propietarioParentUpdate(false);
    }
    
    handleSubmit = (e) => {
        /* const options = {
            headers: { 'Content-Type': 'application/json' },
          };
        axios(options);
         */
        const obj = {id:this.state.idPropietario,
                    nombre:this.state.nombre,
                    apPaterno:this.state.apPaterno,
                    telefono:this.state.telefono,
                    email:this.state.email
                };
        
        e.preventDefault();
        var self=this;
        axios.post(`${data.url}propiedades/propietario/editar`,obj)
        .then(res => {
            const respuesta = res.data;
            //self.setState({isUpdate:respuesta.isUpdate});
            if(respuesta.isUpdate==true){
                self.props.propietarioParentUpdate(false); /** cambia el estado en el padre */  
            }
        });
        
        //
        
    }


    render(){
        return(
            <div className="container"><div className="col s12 m12">
                <h1>Datos Propietario</h1>
                <div className="card horizontal">
                    <div className="card-stacked">
                        <form onSubmit={this.handleSubmit}>
                            <div className="card-content">
                                <p>
                                    <label htmlFor='nombre'>Nombre : </label>
                                    <input 
                                        id='nombre'
                                        name='nombre'
                                        placeholder='Nombre'
                                        onChange={e=>this.setState({nombre:e.target.value})}
                                        ref={inputElement=>this.inputName=inputElement}
                                        value={this.state.nombre}
                                    />
                                </p>
                                <p>
                                    <label htmlFor='apPaterno'>Apellidos : </label>
                                    <input 
                                        id='apPaterno'
                                        name='apPaterno'
                                        placeholder='Apellido'
                                        onChange={e=>this.setState({apPaterno:e.target.value})}
                                        ref={inputElement=>this.inputName=inputElement}
                                        value={this.state.apPaterno}
                                    />
                                </p>
                                <p>
                                    <label htmlFor='telefono'>Telefono : </label>
                                    <input 
                                        id='telefono'
                                        name='telefono'
                                        placeholder='Telefono'
                                        onChange={e=>this.setState({telefono:e.target.value})}
                                        ref={inputElement=>this.inputName=inputElement}
                                        value={this.state.telefono}
                                    />
                                </p>

                                <p>
                                    <label htmlFor='email'>Correo : </label>
                                    <input 
                                        id='email'
                                        name='email'
                                        placeholder='E mail'
                                        onChange={e=>this.setState({email:e.target.value})}
                                        ref={inputElement=>this.inputName=inputElement}
                                        value={this.state.email}
                                    />
                                </p>
                                <div className="card-action"><button className="waves-effect waves-light btn">Siguiente</button></div>
                            </div>
                        </form>
                    </div>
                </div>
                {/*<button onClick={ ()=> this.EditPropietario(this.props.idPropietario)}>Editar Propietario</button>*/}
            </div></div>
        );
    }
    

}