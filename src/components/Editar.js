import React,{Component} from 'react';

import EditarDireccion from './EditarDireccion';
import EditarPropietario from './EditarPropietario';
import EditarPrecio from './EditarPrecio';


import {Link} from 'react-router-dom';

/* import data from '../conf/data';

import axios from 'axios';
import { Link } from "react-router-dom";
 */

export default class Editar extends Component{




    constructor(props){
        super(props);
        this.state={
            idPropietario:this.props.match.params.id,
            showDireccion:true,
            showPropietario:false,
            showPrecio:false,
        }
        this.updateThisDireccion=this.updateThisDireccion.bind(this);
        this.updateThisPropietario=this.updateThisPropietario.bind(this);
        this.updateThisPrecio=this.updateThisPrecio.bind(this);
    }


    updateThisDireccion(){
        this.setState({showDireccion:false});
        this.setState({showPropietario:true});
    }

    updateThisPropietario(){
        this.setState({showPropietario:false});
        this.setState({showPrecio:true});
    }

    updateThisPrecio(){
        this.setState({showPrecio:false});
    }

    render(){
        if( !this.state.showDireccion && !this.state.showPropietario && !this.state.showPrecio ){
            return(
                <div className="container">
                    <div className="col s12 m12">
                        <h1>Finalizar</h1>
                        <p><Link to="/" className="waves-effect waves-light btn">Aceptar</Link></p>
                    </div>
                </div>
            );
        }else{
            return(
                <div>
                {this.state.showDireccion  
                    ? <EditarDireccion  direccionParentUpdate={this.updateThisDireccion} idPropietario={this.state.idPropietario} />
                    : <div></div>}
                {this.state.showPropietario  
                    ? <EditarPropietario  propietarioParentUpdate={this.updateThisPropietario} idPropietario={this.state.idPropietario} />
                    : <div></div>} 
                {this.state.showPrecio  
                    ? <EditarPrecio  precioParentUpdate={this.updateThisPrecio} idPropietario={this.state.idPropietario} />
                    : <div></div>} 
                </div>
            );
            }
    }
    

}