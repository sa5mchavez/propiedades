import React,{Component} from 'react';


import data from '../conf/data';

import axios from 'axios';




/* console.clear(); */
export default class EditarDireccion extends Component{

    constructor(props){
        super(props);
        const idProp = this.props.idPropietario;
        this.state={
            direccion:'',
            numero_int:'',
            delegacion:'',
            numero_ext:'',
            ciudad:'',
            idPropietario:idProp,
            isUpdate:0
        };
    }


    componentDidMount() {
        axios.get(`${data.url}propiedades/${this.props.idPropietario}`,{})
        .then(res => {
          const propiedad = res.data;
          this.setState({ direccion:propiedad.direccion, numero_ext:propiedad.numero_ext,
                        numero_int:propiedad.numero_int,delegacion:propiedad.delegacion,
                        ciudad:propiedad.ciudad });
        });
    
    }

    EditDireccion(id){
        this.props.direccionParentUpdate(false);
    }
    
    handleSubmit = (e) => {
        /* const options = {
            headers: { 'Content-Type': 'application/json' },
          };
        axios(options);
         */
        const obj = {id:this.state.idPropietario,
                    direccion:this.state.direccion,
                    numero_ext:this.state.numero_ext,
                    numero_int:this.state.numero_int,
                    delegacion:this.state.delegacion,
                    ciudad:this.state.ciudad};
        
        e.preventDefault();
        var self=this;
        axios.post(`${data.url}propiedades/direccion/editar`,obj)
        .then(res => {
            const respuesta = res.data;
            //self.setState({isUpdate:respuesta.isUpdate});
            if(respuesta.isUpdate==true){
                self.props.direccionParentUpdate(false); /** cambia el estado en el padre */  
            }
        });
        
        //
        
    }


    render(){
        return(
            <div className="container">
                <div className="col s12 m12">
                <h2 className="header">Datos Direccion</h2>
                <div className="card horizontal">
                    <div className="card-stacked">
                            <form onSubmit={this.handleSubmit}>
                                <div className="card-content">
                                    <p>
                                        <label htmlFor='direccion'>Direccion : </label>
                                        <input 
                                            id='direccion'
                                            name='direccion'
                                            placeholder='Direccion'
                                            onChange={e=>this.setState({direccion:e.target.value})}
                                            ref={inputElement=>this.inputName=inputElement}
                                            value={this.state.direccion}
                                        />
                                    </p>
                                    <p>
                                        <label htmlFor='numero_ext'>Numero Exterior : </label>
                                        <input 
                                            id='numero_ext'
                                            name='numero_ext'
                                            placeholder='Numero Exterior'
                                            onChange={e=>this.setState({numero_ext:e.target.value})}
                                            ref={inputElement=>this.inputName=inputElement}
                                            value={this.state.numero_ext}
                                        />
                                    </p>

                                    <p>
                                        <label htmlFor='numero_int'>Numero Interior : </label>
                                        <input 
                                            id='numero_int'
                                            name='numero_int'
                                            placeholder='Numero Interior'
                                            onChange={e=>this.setState({numero_int:e.target.value})}
                                            ref={inputElement=>this.inputName=inputElement}
                                            value={this.state.numero_int}
                                        />
                                    </p>
                                    <p>
                                        <label htmlFor='delegacion'>Delegacion : </label>
                                        <input 
                                            id='delegacion'
                                            name='delegacion'
                                            placeholder='Delegacion'
                                            onChange={e=>this.setState({delegacion:e.target.value})}
                                            ref={inputElement=>this.inputName=inputElement}
                                            value={this.state.delegacion}
                                        />
                                    </p>
                                    <p>
                                        <label htmlFor='ciudad'>Ciudad : </label>
                                        <input 
                                            id='ciudad'
                                            name='ciudad'
                                            placeholder='Ciudad'
                                            onChange={e=>this.setState({ciudad:e.target.value})}
                                            ref={inputElement=>this.inputName=inputElement}
                                            value={this.state.ciudad}
                                        />
                                    </p>
                                {/*<button onClick={ ()=> this.EditDireccion(this.props.idPropietario)}>Editar Direccion</button>*/}
                                <div className="card-action"><button className="waves-effect waves-light btn">Siguiente</button></div>                    
                            </div>
                        </form>
                    </div>
                </div>    
                </div>
            </div>
        );
    }
    

}